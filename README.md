Welcome to the GitWebhookProxyLib Project
============================

This project is designed to have an easy setup for api calls to different providers such as [Packagist](https://packagist.org) 
or your continous integration server like Jenkins.

This project (or let's say library) is used in the proxy [GitWebhookProxy](https://bitbucket.org/Rebel-L/gitwebhookproxy)
itself based on the lumen framework.

Requirements
============

This project uses composer so the actual requirements you'll find in the *composer.json* file.

Documentation
=============

I'll try to keep my documentation updated, but I don't like to maintain them twice. So in the proxy there is an UML 
diagram for both projects (lib and proxy). So please have a look into the 
[UML](https://bitbucket.org/Rebel-L/gitwebhookproxy/src/066d3c911adebe08676ddca728131f96347860f2/docs/BitbucketWebhookProxy.asta?at=master) 
of the proxy itself.

To have a deeper look into the GitWebhookProxyLib please refer to the *[Wiki Pages](https://bitbucket.org/Rebel-L/gitwebhookproxylib/wiki/)*.
