<?php
/**
 * This is the base class which can be used for all clients.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 11.07.2015
 * Time: 22:04
 */

namespace GitWebhookProxy;

use GitWebhookProxy\ApiClient\ConfigException;
use GitWebhookProxy\ApiClientInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class ApiClientBase implements ApiClientInterface
{
	/**
	 * Exception message if key to search in config is not a string.
	 */
	const MSG_EXCEPTION_CONFIG_KEY = 'The key to search in the config must be a string!';

	/**
	 * Exception message abortig the request because of invalid config for api client.
	 */
	const MSG_EXCEPTION_CONfIG_INVALID =
		'Cannot execute request because api client config is invalid.
		Please have a look into the documentation of the causing api client.';

	/**
	 * Exception message not given a string value as parameter.
	 */
	const MSG_EXCEPTION_NOSTRING = 'For some parameters strings are expected, but get something else';

	/**
	 * The client to execute the API calls.
	 *
	 * @var Client
	 */
	private $client;

	/**
	 * The config for the service.
	 *
	 * @var array
	 */
	private $config;

	/**
	 * Stores the validation error messages.
	 *
	 * @var array
	 */
	private $validationErrors = [];

	/**
	 * The constructor is able to set the config.
	 *
	 * @param array $config [optional] The config to be initialized with.
	 * @return ApiClientBase
	 */
	public function __construct(array $config = [])
	{
		$this->setConfig($config);
	}

	/**
	 * Returns the client on which the API calls are excuted.
	 *
	 * @return Client
	 */
	public function getClient()
	{
		if ($this->client === null) {
			$this->setClient(new Client());
		}

		return $this->client;
	}

	/**
	 * Sets the client on which the API calls are excuted.
	 *
	 * @param Client $client The client object.
	 * @return ApiClientBase
	 */
	public function setClient(Client $client)
	{
		$this->client = $client;
		return $this;
	}

	/**
	 * Returns the config.
	 *
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * Sets the config for the api client.
	 *
	 * @param array $config The config to be set.
	 * @return ApiClientBase
	 */
	public function setConfig(array $config)
	{
		$this->config = $config;
		return $this;
	}

	/**
	 * Returns an array of validation errors. The key is the <b>property</b> which failed validation and the
	 * <b>value</b> is the message describing the failed validation.
	 *
	 * @see GitWebhookProxy\ApiClientInterface::getValidationErrors
	 *
	 * @return array
	 */
	public function getValidationErrors()
	{
		return $this->validationErrors;
	}

	/**
	 * Adds an error message for a given property.
	 *
	 * @param string $property The property which validation failed.
	 * @param string $message The error message describing the failure.
	 *
	 * @throws InvalidArgumentException
	 *
	 * @return ApiClientBase
	 */
	public function addValidationError($property, $message)
	{
		if (is_string($property) === false || is_string($message) === false) {
			throw new \InvalidArgumentException(self::MSG_EXCEPTION_NOSTRING);
		}

		$this->validationErrors[$property] = $message;

		return $this;
	}

	/**
	 * Executes the request to service provider.
	 * <span style="font-weight: bold; color:red;">Should throw GitWebhookProxy\ApiClient\ConfigException if the config
	 * is invalid to transport the error message.</span>
	 *
	 * @see GitWebhookProxy\ApiClientInterface::doRequest
	 *
	 * @throws ConfigException
	 * @return ResponseInterface
	 */
	public function doRequest()
	{
		if ($this->validateConfig() === false) {
			throw new ConfigException(self::MSG_EXCEPTION_CONfIG_INVALID);
		}
		return $this->getClient()->send($this->initRequest());
	}

	/**
	 * Returns the value for a given key in config.
	 * <span style="font-weight: bold; color:red;">Throws \InvalidArgumentException if the key is not a string.</span>
	 *
	 * @param string $key The key in config to return the value for.
	 * @param mixed $default The default to return if key is not found in config.
	 * @return mixed
	 */
	protected function getConfigValue($key, $default = null)
	{
		if (is_string($key) === false) {
			throw new \InvalidArgumentException(self::MSG_EXCEPTION_CONFIG_KEY);
		}

		if ($this->doesConfigKeyExists($key) === false) {
			return $default;
		}

		return $this->getConfig()[$key];
	}

	/**
	 * Checks if the key is set in the config array.
	 * <span style="font-weight: bold; color:red;">Throws \InvalidArgumentException if the key is not a string.</span>
	 *
	 * @param string $key The key to look into the config array.
	 * @throws \InvalidArgumentException
	 * @return bool
	 */
	protected function doesConfigKeyExists($key)
	{
		if (is_string($key) === false) {
			throw new \InvalidArgumentException(self::MSG_EXCEPTION_CONFIG_KEY);
		}
		return array_key_exists($key, $this->getConfig());
	}
}