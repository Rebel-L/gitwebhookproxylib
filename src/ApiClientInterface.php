<?php
/**
 * This is the interface for all API clients.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 11.07.2015
 * Time: 21:46
 */

namespace GitWebhookProxy;

use GitWebhookProxy\ApiClient\ConfigException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


interface ApiClientInterface
{
	/**
	 * Initilizes the http request.
	 *
	 * @return RequestInterface
	 */
	public function initRequest();

	/**
	 * Executing the request against the service provider.
	 * <span style="font-weight: bold; color:red;">Should throw GitWebhookProxy\ApiClient\ConfigException if the config
	 * is invalid to transport the error message.</span>
	 *
	 * @throws ConfigException
	 * @return ResponseInterface
	 */
	public function doRequest();

	/**
	 * Sets the config for the api client.
	 *
	 * @param array $config The config to be set.
	 * @return ApiClientInterface
	 */
	public function setConfig(array $config);

	/**
	 * Validates the config injected to api client.
	 *
	 * @return bool
	 */
	public function validateConfig();

	/**
	 * Returns an array of validation errors. The key is the <b>property</b> which failed validation and the
	 * <b>value</b> is the message describing the failed validation.
	 *
	 * @return array
	 */
	public function getValidationErrors();
}