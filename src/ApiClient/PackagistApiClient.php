<?php
/**
 * This class is the API client to connect to packagist.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 25.07.2015
 * Time: 14:13
 */

namespace GitWebhookProxy\ApiClient;

use GitWebhookProxy\ApiClientBase;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validation;

class PackagistApiClient extends ApiClientBase
{
	/**
	 * The request uri to update packagist.
	 */
	const REQUEST_URI = 'https://packagist.org/api/update-package?username=%s&apiToken=%s';

	/**
	 * The request method how to reach packagist.
	 */
	const REQUEST_METHOD = 'POST';

	/**
	 * Parameter name for config of username.
	 */
	const CONFIG_PARAMETER_USERNAME = 'username';

	/**
	 * Parameter name for config of apiToker.
	 */
	const CONFIG_PARAMETER_APITOKEN = 'apiToken';

	/**
	 * Parameter name for config of repositoryUrl.
	 */
	const CONFIG_PARAMETER_REPOSITORYURL = 'repositoryUrl';

	/**
	 * Initilizes the http request.
	 *
	 * @see GitWebhookProxy\ApiClientInterface::initRequest
	 *
	 * @return RequestInterface
	 */
	public function initRequest()
	{
		return new Request(
			self::REQUEST_METHOD,
			$this->getRequestUri(),
			$this->getRequestHeaders(),
			$this->getRequestBody()
		);
	}

	/**
	 * Validates the config injected to api client.
	 *
	 *  @see GitWebhookProxy\ApiClientInterface::validateConfig
	 *
	 * @return bool
	 */
	public function validateConfig()
	{
		$validator = Validation::createValidator();

		$constraints = new Constraints\Collection([
			self::CONFIG_PARAMETER_USERNAME => [
				new Constraints\Regex('/[\w]+/'),
				new Constraints\Length([
					'min' => 3
				]),
			],
			self::CONFIG_PARAMETER_APITOKEN => [
				new Constraints\Regex('/[\w]+/'),
				new Constraints\Length([
					'min' => 10,
					'max' => 32
				]),
			],
			self::CONFIG_PARAMETER_REPOSITORYURL => [
				new Constraints\Url(),
			]
		]);

		$violations = $validator->validate($this->getConfig(), $constraints);
		/** @var \Symfony\Component\Validator\ConstraintViolation $violation */
		foreach ($violations as $violation) {
			$this->addValidationError($violation->getPropertyPath(), $violation->getMessage());
		}

		if ($violations->count() === 0) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the URI for packagist with username and apiToken.
	 *
	 * @return string
	 */
	private function getRequestUri()
	{
		return sprintf(
			self::REQUEST_URI,
			$this->getConfigValue(self::CONFIG_PARAMETER_USERNAME),
			$this->getConfigValue(self::CONFIG_PARAMETER_APITOKEN)
		);
	}

	/**
	 * Returns the headers for packagist request.
	 *
	 * @return array
	 */
	private function getRequestHeaders()
	{
		return [
			'Content-type' => 'application/json',
			'Accept' => 'application/json'
		];
	}

	/**
	 * Returns the request body.
	 *
	 * @return string
	 */
	private function getRequestBody()
	{
		$subbody = new \stdClass();
		$subbody->url = $this->getConfigValue(self::CONFIG_PARAMETER_REPOSITORYURL);
		$body = new \stdClass();
		$body->repository = $subbody;
		return json_encode($body, JSON_UNESCAPED_SLASHES);
	}
}