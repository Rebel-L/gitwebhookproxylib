<?php
/**
 * A defined exception if the configuration of an api client is not valid.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 * @subpackage ApiClient
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 15.07.2015
 * Time: 22:49
 */

namespace GitWebhookProxy\ApiClient;


class ConfigException extends \UnexpectedValueException
{

}