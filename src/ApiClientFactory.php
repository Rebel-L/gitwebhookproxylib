<?php
/**
 * This factory creates and sets up the API clients.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 26.07.2015
 * Time: 12:11
 */

namespace GitWebhookProxy;

use GitWebhookProxy\ApiClient\PackagistApiClient;

class ApiClientFactory
{
	/**
	 * The API client name for packagist.
	 */
	const API_CLIENT_PACKAGIST = 'PACKAGIST';

	/**
	 * The exception message if the API name is of wrong type.
	 */
	const MSG_EXCEPTION_APINAME = 'API name must be of type string';

	/**
	 * The exception message if there is now implementation of API.
	 */
	const MSG_EXCEPTION_NOAPI = 'There is no implementation for the API "%s"';

	/**
	 * Creates an API client object and does the setup.
	 *
	 * @param string $apiName
	 * @param array $config
	 *
	 * @return \GitWebhookProxy\ApiClientInterface;
	 */
	public function getApiClient($apiName, array $config)
	{
		if (is_string($apiName) !== true) {
			throw new \InvalidArgumentException(self::MSG_EXCEPTION_APINAME);
		}

		/** @var ApiClientInterface $apiClient */
		$apiClient = null;
		switch (strtoupper($apiName)) {
			case self::API_CLIENT_PACKAGIST:
				$apiClient = new PackagistApiClient();
				break;
			default:
				throw new \UnexpectedValueException(sprintf(self::MSG_EXCEPTION_NOAPI, $apiName));
				break;
		}

		$apiClient->setConfig($config);
		return $apiClient;
	}
}