<?php
/**
 * Unittest for the abstract ApiClientBase class.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 * @subpackage Unittests
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 11.07.2015
 * Time: 22:09
 */

namespace GitWebhookProxy\Unittests;

use GitWebhookProxy\ApiClientBase;

class ApiClientBaseTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Tests the mutators for client.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::setClient
	 * @covers GitWebhookProxy\ApiClientBase::getClient
	 *
	 * @return void
	 */
	public function testMutatorsClient()
	{
		$base = $this->getApiClientBaseMock();
		$client = $this->getClientMock();
		$this->assertSame($base, $base->setClient($client), 'Fluent interface not working for setClient()');
		$this->assertSame($client, $base->getClient(), 'Getter should return injected client object.');
	}

	/**
	 * Test executing a request.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doRequest
	 * @return void
	 */
	public function testDoRequest()
	{
		$request = $this->getRequestMock();

		$response = $this->getResponseMock();

		$client = $this->getClientMock(['send']);
		$client->expects($this->once())
			->method('send')
			->with($request)
			->willReturn($response);

		$base = $this->getApiClientBaseMock(['initRequest', 'validateConfig']);
		$base->setClient($client);
		$base->expects($this->once())
			->method('initRequest')
			->willReturn($request);
		$base->expects($this->once())
			->method('validateConfig')
			->willReturn(true);

		$this->assertSame($response, $base->doRequest(), 'doRequest was not able to return a response');
	}

	/**
	 * Test mutators for the config.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getConfig
	 * @covers GitWebhookProxy\ApiClientBase::setConfig
	 * @return void
	 */
	public function testMutatorsConfig()
	{
		$fixture = [
			'myConfig' => 'myValue'
		];

		$base = $this->getApiClientBaseMock();
		$this->assertSame([], $base->getConfig(), 'Config is not initialized with an empty array');
		$this->assertSame($base, $base->setConfig($fixture), 'Fluent interface for set Config does not work');
		$this->assertSame($fixture, $base->getConfig(), 'Was not able to set config');
	}

	/**
	 * Test mutators for the ValidationErrors.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getValidationErrors
	 * @covers GitWebhookProxy\ApiClientBase::addValidationError
	 * @return void
	 */
	public function testMutatorsValidationErrors()
	{
		$fixtures = [
			'property' => 'username',
			'message' => 'Username should not be empty'
		];
		$fixtures['validationErrors'] = [
			$fixtures['property'] => $fixtures['message']
		];

		$base = $this->getApiClientBaseMock();
		$this->assertSame([], $base->getValidationErrors(), 'There should be no validation errors after initialization');
		$this->assertSame(
			$base,
			$base->addValidationError($fixtures['property'], $fixtures['message']),
			'Fluent interface is not working for addValidationError'
		);
		$this->assertSame($fixtures['validationErrors'], $base->getValidationErrors(), 'Adding error messages failed.');
	}

	/**
	 * Tests that addValidationError() throws an exception if parameters are not of type string.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::addValidationError
	 * @return void
	 */
	public function testValidationErrorsFailure()
	{
		$base = $this->getApiClientBaseMock();
		$this->setExpectedException('InvalidArgumentException', ApiClientBase::MSG_EXCEPTION_NOSTRING);
		$base->addValidationError(1, true);
	}

	/**
	 * Tests setting config by contructor.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::__construct
	 * @return void
	 */
	public function testConstructor()
	{
		$fixture = [
			'ConstructorConfig' => 'ConfigValue'
		];

		$builder = $this->getMockBuilder('GitWebhookProxy\ApiClientBase');
		$builder->disableOriginalConstructor();
		$base = $builder->getMockForAbstractClass();
		$base->__construct($fixture);
		$this->assertSame($fixture, $base->getConfig(), 'Config could not be set by constructor');
	}

	/**
	 * Tests that a key is set in the config.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doesConfigKeyExists
	 * @return void
	 */
	public function testDoesConfigKeyExistsSuccess()
	{
		$fixture = [
			'keyExist' => 'andHasValue'
		];
		$base =$this->getApiClientBaseMock();
		$base->setConfig($fixture);
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('doesConfigKeyExists');
		$reflectionMethod->setAccessible(true);
		$this->assertTrue($reflectionMethod->invokeArgs($base, ['keyExist']), 'Cannot locate key in config');
	}

	/**
	 * Tests that a key is not set in the config.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doesConfigKeyExists
	 * @return void
	 */
	public function testDoesConfigKeyExistsFailure()
	{
		$fixture = [
			'keyExist' => 'andHasValue'
		];
		$base =$this->getApiClientBaseMock();
		$base->setConfig($fixture);
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('doesConfigKeyExists');
		$reflectionMethod->setAccessible(true);
		$this->assertFalse($reflectionMethod->invokeArgs($base, ['keyNotExist']), 'Key should be not located in config');
	}

	/**
	 * Tests that an exception is thrown if a the given key is not a string.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doesConfigKeyExists
	 * @return void
	 */
	public function testDoesConfigKeyExistsWrongKey()
	{
		$base =$this->getApiClientBaseMock();
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('doesConfigKeyExists');
		$reflectionMethod->setAccessible(true);

		$this->setExpectedException('InvalidArgumentException', ApiClientBase::MSG_EXCEPTION_CONFIG_KEY);
		$reflectionMethod->invokeArgs($base, [123]);
	}

	/**
	 * Tests that value from config is returned for a given key.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getConfigValue
	 * @return void
	 */
	public function testGetConfigValueSuccess()
	{
		$fixture = [
			'keyExist' => 'andHasValue'
		];
		$base =$this->getApiClientBaseMock();
		$base->setConfig($fixture);
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('getConfigValue');
		$reflectionMethod->setAccessible(true);
		$this->assertSame(
			$fixture['keyExist'],
			$reflectionMethod->invokeArgs($base, ['keyExist']),
			'Cannot get value for key from config'
		);
	}

	/**
	 * Tests that no value from config is returned for a given key.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getConfigValue
	 * @return void
	 */
	public function testGetConfigValueFailure()
	{
		$fixture = [
			'keyExist' => 'andHasValue'
		];
		$base =$this->getApiClientBaseMock();
		$base->setConfig($fixture);
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('getConfigValue');
		$reflectionMethod->setAccessible(true);
		$this->assertNull(
			$reflectionMethod->invokeArgs($base, ['keyNotExist']),
			'Returned wrong value for a not existing key in config'
		);
	}

	/**
	 * Tests that no value from config is returned for a given key returning a user specific default.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getConfigValue
	 * @return void
	 */
	public function testGetConfigValueWithDifferentDefaultFailure()
	{
		$fixture = [
			'keyExist' => 'andHasValue'
		];
		$base =$this->getApiClientBaseMock();
		$base->setConfig($fixture);
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('getConfigValue');
		$reflectionMethod->setAccessible(true);
		$this->assertSame(
			'myDefault',
			$reflectionMethod->invokeArgs($base, ['keyNotExist', 'myDefault']),
			'Returned not the user specific default value for a not existing key in config'
		);
	}

	/**
	 * Tests that an exception is thrown if a the given key is not a string.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getConfigValue
	 * @return void
	 */
	public function testGetConfigValueWrongKey()
	{
		$base =$this->getApiClientBaseMock();
		$reflection = new \ReflectionClass($base);
		$reflectionMethod = $reflection->getMethod('getConfigValue');
		$reflectionMethod->setAccessible(true);

		$this->setExpectedException('InvalidArgumentException', ApiClientBase::MSG_EXCEPTION_CONFIG_KEY);
		$reflectionMethod->invokeArgs($base, [123]);
	}

	/**
	 * Returns a mock object for GuzzleHttp\Client.
	 *
	 * @param array $methods An array of methods to mock.
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function getClientMock(array $methods = [])
	{
		$builder = $this->getMockBuilder('GuzzleHttp\Client');
		$builder->disableAutoload();
		$builder->disableOriginalConstructor();
		if (count($methods) > 0) {
			$builder->setMethods($methods);
		}
		$mock = $builder->getMock();
		return $mock;
	}

	/**
	 * Returns a mock object for abstract ApiClientBase.
	 *
	 * @param array $methods An array of methods to mock.
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function getApiClientBaseMock(array $methods = [])
	{
		$builder = $this->getMockBuilder('GitWebhookProxy\ApiClientBase');
		if (count($methods) > 0) {
			$builder->setMethods($methods);
		}
		return $builder->getMockForAbstractClass();
	}

	/**
	 * Returns a mock for the request.
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function getRequestMock()
	{
		$builder = $this->getMockBuilder('Psr\Http\Message\RequestInterface');
		return $builder->getMockForAbstractClass();
	}

	/**
	 * Returns a mock for the response.
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function getResponseMock()
	{
		$builder = $this->getMockBuilder('Psr\Http\Message\ResponseInterface');
		return $builder->getMockForAbstractClass();
	}
}
