<?php
/**
 * Tests the API client factory.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 * @subpackage IntegrationTests
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 26.07.2015
 * Time: 12:15
 */

namespace GitWebhookProxy\IntegrationTests;

use GitWebhookProxy\ApiClientFactory;

class ApiClientFactoryTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Tests that a Packagist API client is returned successfully.
	 *
	 * @covers \GitWebhookProxy\ApiClientFactory::getApiClient
	 *
	 * @return void
	 */
	public function testGetApiClientPackagistSuccess()
	{
		$config = [
			'username' => 'NewUser',
			'apiToken' => '12g3-a35bc',
			'repositoryUrl' => 'http://newrepo.url'
		];
		$factory = new ApiClientFactory();

		/** @var \GitWebhookProxy\ApiClientBase $apiClient */
		$apiClient = $factory->getApiClient('packagist', $config);
		$this->assertInstanceOf('GitWebhookProxy\ApiClient\PackagistApiClient', $apiClient, 'Factory returned wrong type of API client');
		$this->assertSame($config, $apiClient->getConfig(), 'Config was not set by factory');
	}

	/**
	 * Tests that the API name must be a string.
	 *
	 * @covers \GitWebhookProxy\ApiClientFactory::getApiClient
	 *
	 * @return void
	 */
	public function testGetApiClientWrongApiName()
	{
		$factory = new ApiClientFactory();
		$this->setExpectedException('InvalidArgumentException', ApiClientFactory::MSG_EXCEPTION_APINAME);
		$factory->getApiClient(new \stdClass(), []);
	}

	/**
	 * Tests that an exception is thrown if there is no implementation of API name.
	 *
	 * @covers \GitWebhookProxy\ApiClientFactory::getApiClient
	 *
	 * @return void
	 */
	public function testGetApiClientNotExistingApiName()
	{
		$apiName = 'NotExistingAPI';
		$factory = new ApiClientFactory();
		$this->setExpectedException(
			'UnexpectedValueException',
			sprintf(ApiClientFactory::MSG_EXCEPTION_NOAPI, $apiName)
		);
		$factory->getApiClient($apiName, []);
	}
}