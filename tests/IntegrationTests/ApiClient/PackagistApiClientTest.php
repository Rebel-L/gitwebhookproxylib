<?php
/**
 * Integration test for the packagist api client.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 * @subpackage IntegrationTests
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 25.07.2015
 * Time: 14:51
 */

namespace GitWebhookProxy\IntegrationTests\ApiClient;

use GitWebhookProxy\ApiClient\PackagistApiClient;

class PackagistApiClientTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Test that the request is initialized correctly.
	 *
	 * @covers GitWebhookProxy\ApiClient\PackagistApiClient::initRequest
	 * @covers GitWebhookProxy\ApiClient\PackagistApiClient::getRequestUri
	 * @covers GitWebhookProxy\ApiClient\PackagistApiClient::getRequestHeaders
	 * @covers GitWebhookProxy\ApiClient\PackagistApiClient::getRequestBody
	 *
	 * @return void
	 */
	public function testInitRequest()
	{
		$config = [
			'username' => 'MyUser',
			'apiToken' => '123-abc',
			'repositoryUrl' => 'http://myrepo.url'
		];

		$fixtures = [];
		$fixtures['host'] = 'packagist.org';
		$fixtures['headers'] = [
				'Host' => [$fixtures['host']],
				'Content-type' => ['application/json'],
				'Accept' => ['application/json']
		];
		$fixtures['body'] = '{"repository":{"url":"http://myrepo.url"}}';

		$apiClient = new PackagistApiClient($config);
		$request = $apiClient->initRequest();
		$this->assertInstanceOf('GuzzleHttp\Psr7\Request', $request, 'Initialisation of the request object failed');
		$this->assertSame('POST', $request->getMethod(), 'Wrong method set');
		$this->assertSame('1.1', $request->getProtocolVersion(), 'Wrong protocol set');
		$this->assertSame($fixtures['headers'], $request->getHeaders(), 'Headers are missing or wrong');
		$this->assertSame($fixtures['body'], $request->getBody()->getContents(), 'Body is missing or is wrong');

		// Test uri object
		$uri = $request->getUri();
		$this->assertSame('https', $uri->getScheme(), 'Scheme is not SSL');
		$this->assertSame($fixtures['host'], $uri->getHost(), 'Packagist host is wrong');
		$this->assertSame('/api/update-package', $uri->getPath(), 'Path of uri not set correctly');
		$this->assertSame('username=MyUser&apiToken=123-abc', $uri->getQuery(), 'Query of uri is not set correct');
	}

	/**
	 * Tests the validation of a correct config.
	 *
	 * @covers GitWebhookProxy\ApiClient\PackagistApiClient::validateConfig
	 *
	 * @return void
	 */
	public function testValidateConfigSuccess()
	{
		$config = [
			'username' => 'NewUser',
			'apiToken' => '12g3-a35bc',
			'repositoryUrl' => 'http://newrepo.url'
		];

		$apiClient = new PackagistApiClient();
		$apiClient->setConfig($config);
		$this->assertTrue($apiClient->validateConfig(), 'The config is not valid anymore, but it should');
	}

	/**
	 * Returns wrong configurations to test.
	 *
	 * @return array
	 */
	public function dataProviderWrongConfigs()
	{
		return [
			[
				['username' => 'user'],
				[
					'[apiToken]' => 'This field is missing.',
					'[repositoryUrl]' => 'This field is missing.'
				]
			],
			[
				['apiToken' => 'token'],
				[
					'[username]' => 'This field is missing.',
    				'[apiToken]' => 'This value is too short. It should have 10 characters or more.',
    				'[repositoryUrl]' => 'This field is missing.'
				]
			],
			[
				['repositoryUrl' => 'https://url.de'],
				[
					'[username]' => 'This field is missing.',
					'[apiToken]' => 'This field is missing.'
				]
			]
		];
	}

	/**
	 * Tests the validation of a correct config.
	 *
	 * @param array $config The configuration to test.
	 * @param array $validationErrors The result of the added error messages.
	 *
	 * @dataProvider dataProviderWrongConfigs
	 * @covers       GitWebhookProxy\ApiClient\PackagistApiClient::validateConfig
	 *
	 */
	public function testValidateConfigFailure(array $config, array $validationErrors)
	{
		$apiClient = new PackagistApiClient($config);
		$this->assertFalse($apiClient->validateConfig(), 'Configuration should fail');
		$this->assertSame($validationErrors, $apiClient->getValidationErrors(), 'The error messages are not added correctly');
	}
}