<?php
/**
 * Integrationtests for the abstract ApiClientBase class.
 *
 * @category ApiClient
 * @package GitWebhookProxy
 * @subpackage IntegrationTests
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 11.07.2015
 * Time: 22:36
 */

namespace GitWebhookProxy\IntegrationTests;

use GitWebhookProxy\ApiClientBase;
use GuzzleHttp\Psr7\Request;

class ApiClientBaseTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Test that getter for client. It should set a new client if not initialized.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::getClient
	 *
	 * @return void
	 */
	public function testGetClientUninitialized()
	{
		$base = $this->getApiClientBaseMock();
		$this->assertInstanceOf('GuzzleHttp\Client', $base->getClient(), 'Getter for client does not return a client if uninitialized');
	}

	/**
	 * Test executing a successful request.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doRequest
	 * @return void
	 */
	public function testDoRequestSuccess()
	{
		$request = new Request('GET', 'http://www.google.de');

		$base = $this->getApiClientBaseMock(['initRequest', 'validateConfig']);
		$base->expects($this->once())
			->method('initRequest')
			->willReturn($request);
		$base->expects($this->once())
			->method('validateConfig')
			->willReturn(true);
		$response = $base->doRequest();
		$this->assertSame(200, $response->getStatusCode());
	}

	/**
	 * Test executing a request with failure.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doRequest
	 * @return void
	 */
	public function testDoRequestFailure()
	{
		$request = new Request('GET', 'http://www.google.de/404');

		$base = $this->getApiClientBaseMock(['initRequest', 'validateConfig']);
		$base->expects($this->once())
			->method('initRequest')
			->willReturn($request);
		$base->expects($this->once())
			->method('validateConfig')
			->willReturn(true);
		$this->setExpectedException('GuzzleHttp\Exception\ClientException', 'Client error: 404');
		$base->doRequest();
	}

	/**
	 * Test executing a request with failure because of invalid config.
	 *
	 * @covers GitWebhookProxy\ApiClientBase::doRequest
	 * @return void
	 */
	public function testDoRequestFailureInvalidConfig()
	{
		$base = $this->getApiClientBaseMock(['validateConfig']);
		$base->expects($this->once())
			->method('validateConfig')
			->willReturn(false);
		$this->setExpectedException(
			'GitWebhookProxy\ApiClient\ConfigException',
			ApiClientBase::MSG_EXCEPTION_CONfIG_INVALID
		);
		$base->doRequest();
	}

	/**
	 * Returns a mock object for abstract ApiClientBase.
	 *
	 * @param array $methods An array of methods to mock.
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function getApiClientBaseMock(array $methods = [])
	{
		$builder = $this->getMockBuilder('GitWebhookProxy\ApiClientBase');
		if (count($methods) > 0) {
			$builder->setMethods($methods);
		}
		return $builder->getMockForAbstractClass();
	}
}
