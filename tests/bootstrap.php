<?php
/**
 * This script/static class bootstraps the tests. It is designed to work with both tests: Unittests & IntegrationTests.
 *
 * @category Bootstrap
 * @package Tests
 *
 * @author Rebel-L <dj@rebel@rebel-l.net>
 * @copyright (c) 2015 by Rebel-L <www.rebel-l.net>
 * @license GPL-3.0
 * @license http://opensource.org/licenses/GPL-3.0 GNU GENERAL PUBLIC LICENSE
 *
 * @version 1.0.0
 * @version GIT: $Id$ In development. Very unstable.
 * 
 * Date: 11.07.2015
 * Time: 18:52
 */
class TestBootstrap
{
	/**
	 * Constant giving the root directory of tests.
	 */
	const ROOT_PATH = __DIR__;

	/**
	 * Path to the vendor directory.
	 *
	 * @var string
	 */
	private static $vendorPath = '';

	/**
	 * Method initializes the test environment.
	 *
	 * @return void
	 */
	public static function init()
	{
		self::initPath();
		self::initAutoload();
	}

	/**
	 * Returns the path to vendor directory.
	 *
	 * @return string
	 */
	private static function getVendorPath()
	{
		return self::$vendorPath;
	}

	/**
	 * Ininitalizes the paths.
	 *
	 * @return void
	 */
	private static function initPath()
	{
		self::$vendorPath = realpath(self::ROOT_PATH . '/../') . '/vendor/';
	}

	/**
	 * Initializes the autoloader for vendors.
	 *
	 * @return void
	 */
	private static function initAutoload()
	{
		require_once (self::getVendorPath() . 'autoload.php');
	}
}

TestBootstrap::init();